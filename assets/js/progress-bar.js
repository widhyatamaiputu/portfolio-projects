// Skill-bar JS
$(".skill-bar-data").each(function () {
    $(this).find(".progress-content").animate({ width: $(this).attr("data-percentage") }, 4000);
    $(this).find(".progress-number-mark").animate({ left: $(this).attr("data-percentage") },
        {
            duration: 4000,
            step: function (now, fx) {
                var data = Math.round(now);
                $(this)
                    .find(".percent")
                    .html(data + "%");
            },
        }
    );
});